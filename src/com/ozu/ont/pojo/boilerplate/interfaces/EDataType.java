
package com.ozu.ont.pojo.boilerplate.interfaces;

public enum EDataType {
	STRING(1), DOUBLE(2), BOOLEAN(3), LONG(4);

	private final int value;

	private EDataType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
}
