package com.ozu.ont.pojo.boilerplate.interfaces;

import java.util.Date;

public interface HasInfo {
	
	public String getSource();
	public Date getTime();
	public void setSource(String source);
	public void setTime(Date time);
}
