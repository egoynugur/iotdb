package com.ozu.ont.pojo.boilerplate.sample;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import com.ozu.ont.pojo.boilerplate.impl.ObjectProperty;
import com.ozu.ont.pojo.boilerplate.impl.Thing;
import com.ozu.ont.pojo.boilerplate.interfaces.IThing;

@Entity
public class Owns
    extends ObjectProperty
{

    protected String predicate;
    @OneToOne(cascade = CascadeType.ALL)
    protected Thing subject;
    @OneToOne(cascade = CascadeType.ALL)
    protected Thing object;

    public Owns(String predicate, IThing subject, IThing object) {
        this.predicate = predicate;
        this.subject = (Thing) subject;
        this.object = (Thing) object;
    }

    public String getPredicate() {
        return predicate;
    }

    public IThing getSubject() {
        return subject;
    }

    public IThing getObject() {
        return object;
    }

}
