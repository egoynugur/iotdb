package com.ozu.ont.pojo.boilerplate.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.ozu.database.hibernate.HibernateUtil;
import com.ozu.ont.pojo.boilerplate.interfaces.IObjectProperty;
import com.ozu.ont.pojo.boilerplate.interfaces.IThing;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class ObjectProperty implements IObjectProperty {

	private static final String OBJECT_PROPERTY_PACKAGE = "com.ozu.ont.pojo.generated.property.object.";

	@Id
	@GeneratedValue
	protected int id;

	protected String predicate;
	@OneToOne(cascade = CascadeType.ALL)
	protected Thing subject;
	@OneToOne(cascade = CascadeType.ALL)
	protected Thing object;

	@OneToOne(cascade = CascadeType.ALL)
	private Info info;

	public ObjectProperty() {
	}

	@Override
	public String getSource() {
		return info.getSource();
	}

	@Override
	public Date getTime() {
		return info.getTime();
	}

	@Override
	public void setSource(String source) {
		info.setSource(source);
	}

	@Override
	public void setTime(Date time) {
		info.setTime(time);
	}

	public Info getInfo() {
		return info;
	}

	public void setInfo(Info info) {
		this.info = info;
	}

	public int getId() {
		return id;
	}

	/**
	 * Factory methods
	 */

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static ObjectProperty createObject(String predicate, IThing subject, IThing object) {
		ObjectProperty obj = null;
		try {
			Class cls = Class.forName(OBJECT_PROPERTY_PACKAGE + predicate);
			obj = (ObjectProperty) cls.getConstructor(String.class, IThing.class, IThing.class).newInstance(predicate,
					subject, object);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return obj;
	}

	public static int addObjectPropertyToSession(String predicate, String subject, String object, Session session) {
		Integer sId = Thing.getIndividualId(subject, session);
		Integer oId = Thing.getIndividualId(object, session);
		Integer pId = getNewId(session);
		return createObjectProperty(pId, sId, oId, predicate, session);
	}

	public static int addObjectPropertyToSession(String predicate, Integer subject, Integer object, Session session) {
		return createObjectProperty(getNewId(session), subject, object, predicate, session);
	}

	private static int getNewId(Session session) {
		Integer pId = (Integer) session.createSQLQuery("SELECT max(id)+1 FROM objectproperty").uniqueResult();
		if (pId == null)
			pId = 1;
		return pId;
	}

	private static int createObjectProperty(Integer pId, Integer sId, Integer oId, String predicate, Session session) {
		SQLQuery q = session.createSQLQuery("INSERT INTO objectproperty"
				+ " (id, predicate, subject_id, object_id) VALUES (:pId, :predicate, :sId, :oId)");
		q.setInteger("pId", pId);
		q.setInteger("sId", sId);
		q.setInteger("oId", oId);
		q.setString("predicate", predicate);
		q.executeUpdate();

		q = session.createSQLQuery("INSERT INTO " + predicate + " (id, subjectid, objectid) VALUES (:pId, :sId, :oId)");
		q.setInteger("pId", pId);
		q.setInteger("sId", sId);
		q.setInteger("oId", oId);
		return q.executeUpdate();
	}

	public static int deleteObjectPropertyFromSession(String predicate, String subject, String object,
			Session session) {
		String select = "SELECT id FROM " + predicate + " WHERE subjectid=:subject AND objectid=:object";
		String delete = "DELETE FROM objectproperty WHERE id=:id";

		Integer subjectId = Thing.getIndividualId(subject, session);
		Integer objectId = Thing.getIndividualId(object, session);

		Query q = session.createSQLQuery(select);
		q.setInteger("subject", subjectId);
		q.setInteger("object", objectId);

		Integer id = (Integer) q.uniqueResult();

		/* Cascade ? */
		q = session.createSQLQuery(delete);
		q.setInteger("id", id);

		return q.executeUpdate();
	}

	public static List<Object[]> getObjectPropertiesFrom(String predicate) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			return getObjectPropertiesFrom(predicate, session);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return null;
	}

	public static List<Object[]> getObjectPropertiesFrom(String predicate, Session session) {
		List<Object[]> list = session.createSQLQuery("SELECT subjectid, objectid FROM " + predicate).list();
		return list;
	}
	

	public static ObjectProperty createObject(String predicate, int subjectId, int objectId) {

		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();

		String query = "FROM Thing where id =:id";
		SQLQuery sqlQuery = session.createSQLQuery(query);
		sqlQuery.setInteger("id", subjectId);

		IThing subject = (IThing) sqlQuery.uniqueResult();

		sqlQuery = session.createSQLQuery(query);
		sqlQuery.setInteger("id", objectId);
		IThing object = (IThing) sqlQuery.uniqueResult();

		session.flush();
		session.close();

		return ObjectProperty.createObject(predicate, subject, object);
	}
}
